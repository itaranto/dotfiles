require('vis')

vis.events.subscribe(vis.events.INIT, function()
  -- Window operations
  vis:map(vis.modes.NORMAL, '<M-v>', ':vsplit<Enter>')
  vis:map(vis.modes.NORMAL, '<M-s>', ':split<Enter>')
  vis:map(vis.modes.NORMAL, '<M-q>', ':q<Enter>')

  -- Switch between different windows by their direction
  vis:map(vis.modes.NORMAL, '<M-h>', '<C-w>h')
  vis:map(vis.modes.NORMAL, '<M-j>', '<C-w>j')
  vis:map(vis.modes.NORMAL, '<M-k>', '<C-w>k')
  vis:map(vis.modes.NORMAL, '<M-l>', '<C-w>l')

  -- Cycle between buffers/windows
  vis:map(vis.modes.NORMAL, '<M-n>', '<vis-window-next>')
  vis:map(vis.modes.NORMAL, '<M-p>', '<vis-window-prev>')

  vis:command('set autoindent on')
  vis:command('set expandtab on')
  vis:command('set tabwidth 4')
  vis:command('set theme onedark')
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
  if vis.win.syntax == 'lua' then
    vis:command('set tabwidth 2')
  end

  vis:command('set cursorline on')
  vis:command('set relativenumbers on')
  vis:command('set show-tabs on')
end)
