require('core')
require('neovide')

-- Bootstrap and setup lazy.nvim
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable',
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

require('lazy').setup('plugins', {
  concurrency = vim.loop.available_parallelism() * 2,
  ui = {
    icons = {
      cmd = '',
      config = '',
      event = '',
      ft = '',
      init = '',
      import = '',
      keys = '',
      lazy = '',
      loaded = '',
      not_loaded = '',
      plugin = '',
      runtime = '',
      source = '',
      start = '',
      task = '',
      list = { '', '', '', '' },
    },
  },
  change_detection = { notify = false },
  performance = {
    rtp = {
      reset = false,
    },
  },
})
