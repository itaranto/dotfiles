local noremap = require('utils').noremap

-- Leader key
vim.g.mapleader = ' '

-- Cycle through the argument list
noremap('n', '[a', ':previous<CR>')
noremap('n', ']a', ':next<CR>')
noremap('n', '[A', ':first<CR>')
noremap('n', ']A', ':last<CR>')

-- Cycle through the buffer list
noremap('n', '[b', ':bprevious<CR>')
noremap('n', ']b', ':bnext<CR>')
noremap('n', '[B', ':bfirst<CR>')
noremap('n', ']B', ':blast<CR>')

-- Cycle through the quickfist list
noremap('n', '[q', ':cprevious<CR>')
noremap('n', ']q', ':cnext<CR>')
noremap('n', '[Q', ':cfirst<CR>')
noremap('n', ']Q', ':clast<CR>')

-- Delete buffer
noremap({ 'n', 't' }, '<A-d>', '<C-\\><C-n>:bd<CR>')

-- Window operations
noremap({ 'n', 'v', 'i', 't' }, '<A-s>', '<C-\\><C-n>:wincmd s<CR>')
noremap({ 'n', 'v', 'i', 't' }, '<A-v>', '<C-\\><C-n>:wincmd v<CR>')
noremap({ 'n', 'v', 'i', 't' }, '<A-q>', '<C-\\><C-n>:wincmd q<CR>')
noremap({ 'n', 'v', 'i', 't' }, '<A-o>', '<C-\\><C-n>:wincmd o<CR>')

-- Switch between different windows by their direction
noremap({ 'n', 'v', 'i', 't' }, '<A-h>', '<C-\\><C-n>:wincmd h<CR>')
noremap({ 'n', 'v', 'i', 't' }, '<A-j>', '<C-\\><C-n>:wincmd j<CR>')
noremap({ 'n', 'v', 'i', 't' }, '<A-k>', '<C-\\><C-n>:wincmd k<CR>')
noremap({ 'n', 'v', 'i', 't' }, '<A-l>', '<C-\\><C-n>:wincmd l<CR>')

-- Escape the terminal
noremap('t', '<C-Space><Esc>', '<C-\\><C-n>')

-- Open a new terminal
noremap('n', '<A-t>', ':split|resize 20|edit term://fish<CR>')

-- Make (n)j and (n)k to be added into the jumplist.
local function add_to_jumplist(key)
  noremap({ 'n', 'x' }, key, function()
    local count = vim.v.count
    return (count > 1 and "m'" .. count or '') .. key
  end, { expr = true })
end

add_to_jumplist('j')
add_to_jumplist('k')

-- Diagnostics.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions.
noremap('n', '<Leader>e', vim.diagnostic.open_float)
noremap('n', '<Leader>d', vim.diagnostic.setloclist)
noremap('n', '[d', vim.diagnostic.goto_prev)
noremap('n', ']d', vim.diagnostic.goto_next)
