vim.opt.breakindent = true
vim.opt.cursorline = true
vim.opt.expandtab = true
vim.opt.fillchars = { fold = ' ', eob = ' ' }
vim.opt.foldlevelstart = 99
vim.opt.guifont = 'Source Code Pro,Font Awesome 6 Free Solid,Font Awesome 6 Brands:h12.5:h12.5:h12.5'
vim.opt.hlsearch = false
vim.opt.laststatus = 3
vim.opt.list = true
vim.opt.listchars = { tab = '» ', trail = '·', extends = '→', precedes = '←', nbsp = '·' }
vim.opt.mouse = 'nvi'
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.shiftwidth = 4
vim.opt.shortmess:append('I')
vim.opt.showmode = false
vim.opt.showtabline = 0
vim.opt.smartindent = true
vim.opt.softtabstop = 4
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.tabstop = 4
vim.opt.undofile = true
vim.opt.visualbell = true
vim.opt.wrap = false

-- Schedule the setting after `UIEnter` because it can increase startup-time.
vim.schedule(function()
  vim.opt.clipboard:append('unnamedplus')
end)

-- Workaround for Neovide.
-- See: https://github.com/neovide/neovide/issues/2004
vim.opt.title = true

-- Highlight text after column
vim.cmd([[match ErrorMsg '\%>100v.\+']])

-- Detect ".h" headers as C files
vim.g.c_syntax_for_h = 1

-- Disable remote plugin warnings
vim.g.loaded_node_provider = 0
vim.g.loaded_perl_provider = 0
vim.g.loaded_python3_provider = 0
vim.g.loaded_ruby_provider = 0

-- Diagnostics
vim.diagnostic.config({
  float = {
    source = true,
  },
})
