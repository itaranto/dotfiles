local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

local group = augroup('InitAutoCommands', {})

-- Start terminal in 'Terminal' mode. I deliberately didn't use 'TermOpen' since it will only
-- work the first time the terminal window is opened.
autocmd(
  { 'BufWinEnter', 'WinEnter' },
  { group = group, pattern = 'term://*', command = 'startinsert' }
)

-- Only highlight matches when searching
autocmd('CmdlineEnter', { group = group, pattern = '/,?', command = ':set hlsearch' })
autocmd('CmdlineLeave', { group = group, pattern = '/,?', command = ':set nohlsearch' })

-- Highlight on yank
autocmd('TextYankPost', {
  group = group,
  pattern = '*',
  callback = function() vim.highlight.on_yank() end,
})

-- Open the quickfix/location window for quickfix/location related commands.
autocmd('QuickFixCmdPost', { group = group, pattern = '[^l]*', command = ':cwindow' })
autocmd('QuickFixCmdPost', { group = group, pattern = 'l*', command = ':lwindow' })
