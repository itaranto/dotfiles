if vim.g.neovide then
  vim.g.neovide_cursor_animation_length = 0.04
  vim.g.neovide_hide_mouse_when_typing = true
  vim.g.neovide_scale_factor = 1.0
  vim.g.neovide_transparency = 0.98

  local function change_scale_factor(delta)
    vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
  end

  local noremap = require('utils').noremap
  noremap('n', '<Leader>=', function() change_scale_factor(1.10) end)
  noremap('n', '<Leader>-', function() change_scale_factor(1 / 1.10) end)
end
