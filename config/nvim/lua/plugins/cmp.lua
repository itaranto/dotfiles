return {
  'hrsh7th/nvim-cmp',
  version = '*',
  dependencies = {
    'f3fora/cmp-spell',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-calc',
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-nvim-lua',
    'hrsh7th/cmp-path',
    'L3MON4D3/LuaSnip',
    'saadparwaiz1/cmp_luasnip',
  },
  config = function()
    local cmp = require('cmp')
    local luasnip = require('luasnip')

    -- nvim-cmp setup
    cmp.setup({
      formatting = {
        format = function(entry, vim_item)
          vim_item.menu = ({
            nvim_lsp = '[LSP]',
            nvim_lua = '[Lua]',
            luasnip = '[Snippet]',
            calc = '[Calc]',
            path = '[Path]',
            spell = '[Spell]',
            buffer = '[Buffer]',
          })[entry.source.name]
          return vim_item
        end,
      },
      mapping = {
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<C-d>'] = cmp.mapping.scroll_docs(4),
        ['<C-u>'] = cmp.mapping.scroll_docs(-4),
        ['<CR>'] = cmp.mapping.confirm({
          behavior = cmp.ConfirmBehavior.Insert,
          select = true,
        }),
        ['<Tab>'] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.select_next_item()
          elseif luasnip.expand_or_locally_jumpable() then
            luasnip.expand_or_jump()
          else
            fallback()
          end
        end, { 'i', 's' }),
        ['<S-Tab>'] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.select_prev_item()
          elseif luasnip.locally_jumpable(-1) then
            luasnip.jump(-1)
          else
            fallback()
          end
        end, { 'i', 's' }),
      },
      snippet = {
        expand = function(args) luasnip.lsp_expand(args.body) end,
      },
      sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'nvim_lua' },
        { name = 'luasnip' },
        { name = 'calc' },
        { name = 'path' },
        {
          name = 'spell',
          option = {
            keep_all_entries = true,
          },
        },
      }, {
        {
          name = 'buffer',
          option = {
            get_bufnrs = function()
              local bufs = {}
              for _, win in ipairs(vim.api.nvim_list_wins()) do
                bufs[vim.api.nvim_win_get_buf(win)] = true
              end
              return vim.tbl_keys(bufs)
            end,
          },
        },
      }),
      -- Disable LSP preselect feature and set completeopt to enabme Vim's preselect feature.
      -- This is to work around a gopls issue where the first entry is not preselected.
      preselect = cmp.PreselectMode.None,
      completion = {
        completeopt = 'menu,menuone,preview',
      },
    })
  end,
}
