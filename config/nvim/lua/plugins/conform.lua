return {
  'stevearc/conform.nvim',
  version = '*',
  event = { 'BufWritePre' },
  cmd = { 'ConformInfo' },
  opts = {
    formatters_by_ft = {
      python = { 'ruff_organize_imports', 'ruff_format' },
      javascript = { 'prettier' },
      typescript = { 'prettier' },
      sql = { 'sqlfluff' },
    },
    format_on_save = {
      timeout_ms = 2000,
      lsp_fallback = true,
    },
  },
}
