return {
  'neovim/nvim-lspconfig',
  version = '*',
  config = function()
    local lspconfig = require('lspconfig')
    local noremap = require('utils').noremap

    -- Add additional capabilities supported by nvim-cmp.
    local capabilities = vim.tbl_deep_extend(
      'force',
      vim.lsp.protocol.make_client_capabilities(),
      require('cmp_nvim_lsp').default_capabilities()
    )

    -- Use LspAttach autocommand to only map the following keys after the language server attaches
    -- to the current buffer.
    vim.api.nvim_create_autocmd('LspAttach', {
      group = vim.api.nvim_create_augroup('LspConfig', {}),
      callback = function(args)
        local client = vim.lsp.get_client_by_id(args.data.client_id)
        assert(client ~= nil)

        -- Diable semantic highlighting.
        client.server_capabilities.semanticTokensProvider = nil

        local telescope_builtin = require('telescope.builtin')

        -- Buffer local mappings.
        -- See `:help vim.lsp.*` for documentation on any of the below functions.
        local buf_opts = { buffer = args.buf }
        local picker_opts = { show_line = false }
        -- Navigation
        noremap('n', 'gD', vim.lsp.buf.declaration, buf_opts)
        noremap('n', 'gd', function() telescope_builtin.lsp_definitions(picker_opts) end, buf_opts)
        noremap('n', 'gi', function() telescope_builtin.lsp_implementations(picker_opts) end, buf_opts)
        noremap('n', 'gr', function() telescope_builtin.lsp_references(picker_opts) end, buf_opts)
        noremap('n', '<Leader>D', function() telescope_builtin.lsp_type_definitions(picker_opts) end, buf_opts)
        -- Documentation
        noremap('n', '<C-k>', vim.lsp.buf.signature_help, buf_opts)
        -- Workspaces
        noremap('n', '<Leader>wa', vim.lsp.buf.add_workspace_folder, buf_opts)
        noremap('n', '<Leader>wr', vim.lsp.buf.remove_workspace_folder, buf_opts)
        noremap(
          'n',
          '<Leader>wl',
          function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end,
          buf_opts
        )
        -- Actions
        noremap('n', '<Leader>r', vim.lsp.buf.rename, buf_opts)
        noremap('n', '<Leader>ca', vim.lsp.buf.code_action, buf_opts)
        noremap('n', '<Leader>m', function() vim.lsp.buf.format({ async = true }) end, buf_opts)
      end,
    })

    -- Use a loop to conveniently call 'setup' on multiple servers and map buffer local keybindings
    -- when the language server attaches.
    local servers = { 'clangd', 'rust_analyzer' }
    for _, lsp in ipairs(servers) do
      lspconfig[lsp].setup({
        capabilities = capabilities,
      })
    end

    lspconfig.gopls.setup({
      capabilities = capabilities,
      settings = {
        gopls = {
          analyses = {
            nilness = true,
            unusedparams = true,
            unusedvariable = true,
            unusedwrite = true,
            useany = true,
          },
          gofumpt = true,
          staticcheck = true,
          usePlaceholders = true,
        },
      },
    })

    lspconfig.lua_ls.setup({
      capabilities = capabilities,
      on_init = function(client)
        local path = client.workspace_folders[1].name
        if vim.loop.fs_stat(path .. '/.luarc.json') or
            vim.loop.fs_stat(path .. '/.luarc.jsonc')
        then
          return
        end

        client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
          runtime = {
            -- Tell the language server which version of Lua you're using. Neovim uses LuaJIT.
            version = 'LuaJIT',
          },
          -- Make the server aware of Neovim runtime files.
          workspace = {
            -- Don't check third party modules. This gets rid of that annoying popup.
            checkThirdParty = false,
            -- Don't look for modules inside Neovim's "plugin" and "after/plugin" directories.
            ignoreDir = { 'plugin/*' },
            -- Make the server aware of Neovim runtime files
            library = vim.api.nvim_get_runtime_file('', true),
          },
        })
      end,
      settings = {
        Lua = {},
      },
    })

    lspconfig.pylsp.setup({
      capabilities = capabilities,
      settings = {
        pylsp = {
          plugins = {
            autopep8 = { enabled = false },
            flake8 = { enabled = false },
            mccabe = { enabled = false },
            pycodestyle = { enabled = false },
            pydocstyle = { enabled = false },
            pyflakes = { enabled = false },
            pylint = { enabled = false },
            pylsp_mypy = {
              enabled = true,
              dmypy = false,
              live_mode = true,
            },
            yapf = { enabled = false },
          },
        },
      },
    })

    lspconfig.ts_ls.setup({
      init_options = {
        maxTsServerMemory = 4096,
      },
      capabilities = capabilities,
      on_attach = function(client, _)
        client.server_capabilities.documentFormattingProvider = false
        client.server_capabilities.documentRangeFormattingProvider = false
      end,
    })
  end,
}
