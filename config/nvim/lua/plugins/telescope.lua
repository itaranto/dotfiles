return {
  'nvim-telescope/telescope.nvim',
  version = '*',
  dependencies = { 'nvim-lua/plenary.nvim', 'nvim-telescope/telescope-fzy-native.nvim' },
  config = function()
    local noremap = require('utils').noremap
    local telescope_builtin = require('telescope.builtin')

    noremap('n', '<Leader>f', telescope_builtin.find_files)
    noremap('n', '<Leader>g', telescope_builtin.live_grep)
    noremap('n', '<Leader>b', telescope_builtin.buffers)

    require('telescope').setup({
      defaults = {
        layout_config = {
          horizontal = {
            preview_width = 0.6,
          },
        },
        path_display = { "smart" },
      },
    })

    require('telescope').load_extension('fzy_native')
  end,
}
