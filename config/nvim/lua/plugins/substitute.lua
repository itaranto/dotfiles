return {
  'gbprod/substitute.nvim',
  version = '*',
  config = function()
    local noremap = require('utils').noremap
    local substitute = require('substitute')
    local substitute_range = require('substitute.range')

    substitute.setup({
      highlight_substituted_text = {
        enabled = false,
      },
    })

    -- No problem since 'S' = 'cc', 's' = 'cl' and 'ss' is not a default mapping
    noremap('n', 'S', substitute.eol)
    noremap('n', 's', substitute.operator)
    noremap('n', 'ss', substitute.line)
    noremap('x', 's', substitute.visual)

    noremap('n', '<Leader>s', substitute_range.operator)
    noremap('n', '<Leader>ss', substitute_range.word)
    noremap('x', '<Leader>s', substitute_range.visual)
  end,
}
