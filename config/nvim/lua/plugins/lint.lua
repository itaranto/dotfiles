return {
  'mfussenegger/nvim-lint',
  commit = '6e9dd545a1af204c4022a8fcd99727ea41ffdcc8',
  config = function()
    local lint = require('lint')

    lint.linters_by_ft = {
      bash = { 'shellcheck' },
      gitcommit = { 'gitlint' },
      javascript = { 'eslint_d' },
      javascriptreact = { 'eslint_d' },
      json = { 'jq' },
      make = { 'checkmake' },
      python = { 'ruff' },
      sh = { 'shellcheck' },
      sql = { 'sqlfluff' },
      yaml = { 'yamllint' },
    }

    vim.api.nvim_create_autocmd({ 'BufEnter', 'BufWritePost', 'InsertLeave', 'TextChanged' }, {
      group = vim.api.nvim_create_augroup('LintConfig', {}),
      callback = function() lint.try_lint(nil, { ignore_errors = true }) end,
    })
  end,
}
