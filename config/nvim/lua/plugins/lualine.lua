return {
  'nvim-lualine/lualine.nvim',
  opts = {
    options = {
      component_separators = { left = '│', right = '│' },
      globalstatus = true,
      icons_enabled = false,
      section_separators = '',
      -- This is purposely not setting theme to 'auto' since this makes lualine to load the them twice:
      -- * 1st, when "setup" is called.
      -- * 2nd, when the colorscheme is changed.
      theme = vim.env.DISPLAY == nil and '16color' or 'onedark',
    },
    sections = {
      lualine_c = {
        {
          'filename',
          symbols = { modified = '*', readonly = 'x' },
        },
      },
    },
    tabline = {},
    winbar = {
      lualine_a = { 'tabs' },
      lualine_b = {
        {
          'filename',
          symbols = { modified = '*', readonly = 'x' },
          color = 'Pmenu',
        }
      },
      lualine_c = {},
      lualine_x = {},
      lualine_y = {},
      lualine_z = {}
    },
    inactive_winbar = {
      lualine_a = { 'tabs' },
      lualine_b = {
        {
          'filename',
          symbols = { modified = '*', readonly = 'x' },
          color = 'SignColumn',
        }
      },
      lualine_c = {},
      lualine_x = {},
      lualine_y = {},
      lualine_z = {}
    }
  }
}
