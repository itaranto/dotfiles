return {
  'https://gitlab.com/itaranto/preview.nvim',
  dependencies = {
    "aklt/plantuml-syntax",
  },
  opts = {
    previewers_by_ft = {
      plantuml = {
        name = 'plantuml_svg',
        renderer = { type = 'imv' },
      },
      groff = {
        name = 'groff_ms_pdf',
        renderer = { type = 'command', opts = { cmd = { 'zathura' } } },
      },
    },
    render_on_write = true,
  },
}
