return {
  'mfussenegger/nvim-jdtls',
  commit = 'ece818f909c6414cbad4e1fb240d87e003e10fda',
  config = function()
    local function get_client_config()
      local home_dir = os.getenv('HOME')
      local install_dir = home_dir .. '/.local/share/jdtls'

      local project_name = vim.fs.basename(vim.fs.root(0, { '.git' }))
      local workspace_dir = home_dir .. '/.cache/jdtls/workspace/' .. project_name

      -- Add additional capabilities supported by nvim-cmp.
      local capabilities = vim.tbl_deep_extend(
        'force',
        vim.lsp.protocol.make_client_capabilities(),
        require('cmp_nvim_lsp').default_capabilities()
      )

      return {
        cmd = {
          'java',
          '-Declipse.application=org.eclipse.jdt.ls.core.id1',
          '-Dosgi.bundles.defaultStartLevel=4',
          '-Declipse.product=org.eclipse.jdt.ls.core.product',
          '-Dlog.protocol=true',
          '-Dlog.level=ALL',
          '-Xmx1g',
          '--add-modules=ALL-SYSTEM',
          '--add-opens', 'java.base/java.util=ALL-UNNAMED',
          '--add-opens', 'java.base/java.lang=ALL-UNNAMED',
          '-jar', install_dir .. '/plugins/org.eclipse.equinox.launcher_1.6.900.v20240613-2009.jar',
          '-configuration', install_dir .. '/config_linux',
          '-data', workspace_dir,
        },
        capabilities = capabilities,
        settings = {
          java = {
            configuration = {
              runtimes = {
                { name = 'JavaSE-11', path = '/usr/lib/jvm/java-11-openjdk/' },
                { name = 'JavaSE-17', path = '/usr/lib/jvm/java-17-openjdk/' },
                { name = 'JavaSE-21', path = '/usr/lib/jvm/java-21-openjdk/' },
              },
              maven = {
                userSettings = home_dir .. '/.config/maven/settings.xml',
                globalSettings = '/etc/maven/settings.xml',
              },
            },
          },
        },
        init_options = {
          bundles = {},
        },
      }
    end

    vim.api.nvim_create_autocmd("Filetype", {
      pattern = "java",
      callback = function()
        require("jdtls").start_or_attach(get_client_config())
      end,
    })
  end,
}
