return {
  'nvim-treesitter/nvim-treesitter',
  commit = '28591731d84c2fc18ddda60e1d53da24c31c4987',
  build = ':TSUpdate',
  config = function()
    require('nvim-treesitter.configs').setup({
      ensure_installed = 'all',
      ignore_install = { 'comment', 'dockerfile', 'hoon' },
      highlight = {
        enable = true,
        disable = { 'comment', 'dockerfile', 'hoon' },
      },
    })

    vim.wo.foldmethod = 'expr'
    vim.wo.foldexpr = 'v:lua.vim.treesitter.foldexpr()'
  end,
}
