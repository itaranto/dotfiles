return {
  'navarasu/onedark.nvim',
  priority = 1000,
  config = function()
    -- Only set the coloscheme when not in a TTY.
    local is_tty = vim.api.nvim_list_uis()[1].term_name == "linux"
    if not is_tty then
      require('onedark').load()
    end
  end,
}
