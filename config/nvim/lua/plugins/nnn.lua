return {
  'luukvbaal/nnn.nvim',
  config = function()
    local noremap = require('utils').noremap

    require('nnn').setup({
      picker = {
        style = {
          width = 0.8,
          height = 0.8,
          border = 'rounded',
        },
        fullscreen = false,
      },
      replace_netrw = 'picker',
    })

    -- Start nnn in the current file's directory
    noremap('n', '<Leader>n', ':NnnPicker %:p:h<CR>')

    -- Match telescope's colors
    -- This assumes the desired colorscheme is already set.
    vim.api.nvim_set_hl(0, 'NnnBorder', { link = 'Constant' })
  end,
}
