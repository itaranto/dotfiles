local M = {}

local function keymap(base_opts)
  return function(mode, lhs, rhs, opts)
    opts = vim.tbl_extend('force', base_opts, opts or {})
    vim.keymap.set(mode, lhs, rhs, opts)
  end
end

M.noremap = keymap({ silent = true })
M.map = keymap({ silent = true, noremap = false })

return M
