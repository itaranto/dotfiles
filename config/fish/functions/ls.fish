source $__fish_data_dir/functions/ls.fish
functions --copy ls fish_ls
function ls
    fish_ls -v --group-directories-first --time-style=long-iso $argv
end
