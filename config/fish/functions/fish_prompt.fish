function fish_prompt --description 'Write out the prompt'
    set -l last_pipestatus $pipestatus

    # Default colors.
    set -l color_user --bold $fish_color_user
    set -l color_cwd --bold $fish_color_cwd
    set -l color_host --bold $fish_color_host
    set -l color_vcs --bold magenta
    set -l color_status --bold $fish_color_status

    # Color the prompt differently when we're root.
    set -l suffix '$'
    if contains -- $USER root toor
        set color_user --bold red
        set suffix '#'
    end

    # Write pipestatus.
    set -l prompt_status (__fish_print_pipestatus ' [' ']' '|' \
        (set_color $color_status) (set_color $color_status) $last_pipestatus)

    echo -n -s \
        (set_color $color_cwd) (prompt_pwd) \
        (set_color $color_vcs) (fish_vcs_prompt) \
        $prompt_status ' '  \
        (set_color $color_user) $suffix \
        # Workarround to unset bold.
        (set_color normal) \
        (set_color $fish_color_normal) ' '
end
