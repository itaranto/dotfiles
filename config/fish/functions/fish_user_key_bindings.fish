function fish_user_key_bindings
    bind yy fish_clipboard_copy
    bind p fish_clipboard_paste
end
