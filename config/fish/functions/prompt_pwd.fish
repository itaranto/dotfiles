function prompt_pwd --description 'Print the current working directory'
    # Replace $HOME with "~".
    set realhome ~
    echo (basename (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD))
end
