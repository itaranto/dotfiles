abbr --add - 'cd -'

alias audacity='env GDK_DPI_SCALE=1.25 GDK_BACKEND=x11 UBUNTU_MENUPROXY=0 audacity'
alias cp='cp -i'
alias g=git
alias info='info --vi-keys'
alias ip='ip -color=auto'
alias l=ls
alias lsblk='lsblk -o model,type,name,size,mountpoints,fstype,label,uuid,parttypename,partlabel,partuuid,ptuuid'
alias mvn="mvn -s $HOME/.config/maven/settings.xml"
alias mongosh="mongosh --norc --shell $HOME/.config/mongoshrc.js"
alias mv='mv -i'
alias n='env SHELL=fish nnn'
alias pacdiff='env DIFFPROG=\'nvim -d\' pacdiff'
alias rm='rm -i'
alias steam='env GDK_DPI_SCALE=1.25 steam'
alias v=nvim
alias vi=nvim
alias vim=nvim
alias vimdiff='nvim -d'
alias vv=neovide
alias wget="wget --hsts-file=$HOME/.local/share/wget-hsts"

if type -q pyenv
    pyenv init - --no-rehash | source
end
