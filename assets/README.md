# Background image

- Title: Tassili n'Ajjer National Park, Algeria
- Author: Chettouh Nabil
- License: CC BY-SA 4.0
- Source: https://commons.wikimedia.org/wiki/User:Chettouh_Nabil#/media/File:Sunrise_Djanet.jpg
