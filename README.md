# dotfiles

My Linux dotfiles.

It contains the user configuration for the software I use. This repository is meant to be paired
with: https://gitlab.com/itaranto/sysconfig

My current setup involves using Arch Linux for my desktop computer and Fedora Linux for my work
laptop. It includes the Sway Wayland compositor as the primary desktop and the i3 X11 Window Manager
as a backup desktop; both use the same base configuration.

Additionally, greetd is used as a login manager.

More detailed information about my current hardware is in the `sysconfig` repository.

## Screenshots

![](./assets/sway_background.webp)

![](./assets/sway_neovide.webp)

![](./assets/sway_pcmanfm-qt_cantata.webp)

![](./assets/sway_zathura_tabs.webp)

![](./assets/sway_firefox.webp)
